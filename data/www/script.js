// mesh topology:
const exampleMesh = [{
    "nodeId": 757307929, "subs": [
        { "nodeId": 2138241514, "subs": [] },
        { "nodeId": 757307466, "subs": [] }
    ]
}, {
    "nodeId": 757308244, "subs": [
        { "nodeId": 3154639577, "subs": [] }
    ]
}, {
    "nodeId": 3954439712, "subs": []
}];

$(() => {
    // load config
    $.get("/config.json", (data) => {
        $('.js-config').val(JSON.stringify(data, null, 4));
    });
    // add handlers
    $('.js-restart').click(() => {
        $.post('/restart');
        alert('restarting...');
    });
});