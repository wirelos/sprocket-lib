# Sprocket-Core
A lightweight Arduino framework for event driven programming.  
  
## Concepts
... topic based event channel / pubsub-pattern  
... plugin system


## Sprocket Lifecycle
TODO

# Useful commands
```sh
# erase flash  
esptool --port /dev/ttyUSB0 erase_flash
  
# OTA
~/.platformio/packages/tool-espotapy/espota.py -i <espIP> -p 8266 -a <authPW> -f .pioenvs/ota/firmware.bin 

```