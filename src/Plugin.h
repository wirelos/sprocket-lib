#ifndef __SPROCKET_PLUGIN__
#define __SPROCKET_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

#include <TaskSchedulerDeclarations.h>
#include <Network.h>
#include <SprocketMessage.h>
#include <EventChannel.h>

class Plugin {
    public:
        EventChannel* eventChannel;
        virtual void activate(Scheduler*);
        virtual void enable(){}
        virtual void disable(){}
        virtual void onMessage(SprocketMessage msg){}
        Plugin* mount(EventChannel* ec) {
            eventChannel = ec;
            return this;
        }
        void subscribe(String topic, subscriptionHandler_t handler){
            eventChannel->subscribe(topic, handler);
        }
        void publish(String topic, String str){
            eventChannel->publish(topic, str);
        }
};

#endif