#ifndef __EXAMPLE_APP__
#define __EXAMPLE_APP__

#include <Sprocket.h>

using namespace std;
using namespace std::placeholders;

class ExampleApp : public Sprocket {
    public:
        Task someTask;
        ExampleApp(SprocketConfig cfg) : Sprocket(cfg) {
            Serial.println("init");
        }
        Sprocket* activate(Scheduler* scheduler) {
            Serial.println("activate");
            someTask.set(TASK_SECOND, TASK_FOREVER, [](){
                Serial.println("do stuff in task");
            });
            scheduler->addTask(someTask);
            someTask.enable();
            return this;
        } using Sprocket::activate;
};

#endif