#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#define SERIAL_BAUD_RATE 115200
#define STARTUP_DELAY 3000

#include "ExampleApp.cpp"

ExampleApp sprocket({ STARTUP_DELAY, SERIAL_BAUD_RATE });

void setup() {
    sprocket.activate();
}

void loop() {
    sprocket.loop();
    yield();
}