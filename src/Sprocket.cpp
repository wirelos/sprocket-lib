#include "Sprocket.h"

Sprocket::Sprocket(){
    Serial.println("init sprocket");
}

Sprocket::Sprocket(SprocketConfig cfg){
    config = cfg;
    init(cfg);
}

Sprocket* Sprocket::init(SprocketConfig cfg){
    delay(cfg.startupDelay);
    Serial.begin(cfg.serialBaudRate);
    SPIFFS.begin();
    scheduler = new Scheduler();
    return this;
}
Sprocket* Sprocket::activate() {
    activatePlugins(scheduler);
    return activate(scheduler);
}

Sprocket* Sprocket::addTask(Task& tsk){
    scheduler->addTask(tsk);
    tsk.enable();
    return this;
}

void Sprocket::loop(){
    scheduler->execute();
}

void Sprocket::dispatch( uint32_t from, String &msg ) {
    currentMessage.fromJsonString(msg);
    if(currentMessage.valid){
        currentMessage.from = from;
        publish(currentMessage.topic, currentMessage.payload);
    }
}

void Sprocket::addPlugin(Plugin* p){
    p->mount(this);
    plugins.reserve(1);
    plugins.push_back(p);
}

void Sprocket::activatePlugins(Scheduler* scheduler){
    for(Plugin* p : plugins){
        p->activate(scheduler);
    }
}
