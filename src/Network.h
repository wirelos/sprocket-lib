#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <Arduino.h>
#include <TaskSchedulerDeclarations.h>
#include <functional>

typedef std::function<void(uint32_t from, String &msg)> msgReceived_cb;

class Network {
    public:
        uint32_t id = 0;
        Scheduler* scheduler;
        virtual Network* init() { return this; };
        virtual Network* init(Scheduler* s) { scheduler = s; return init(); };
        virtual int connect() { return 0; };
        virtual int connectStation() { return 0; };
        virtual int isConnected(){ return 0; };
        virtual void update() {};
        virtual void broadcast(String msg, bool self = false){};
        virtual void sendTo(uint32_t target, String msg) {};
        virtual void onReceive(std::function<void(uint32_t from, String &msg)>) {};
        Network* setScheduler(Scheduler* s) {
            scheduler = s;
            return this;
        }
};

#endif