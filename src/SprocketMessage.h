#ifndef __SPROCKET_MESSAGE__
#define __SPROCKET_MESSAGE__

#include <Arduino.h>
#include <ArduinoJson.h>
#include <JsonStruct.h>

#define JSON_DOMAIN "domain"
#define JSON_FROM "from"
#define JSON_TO "target"
#define JSON_PAYLOAD "payload"
#define JSON_TYPE "type"
#define JSON_TOPIC "topic"
#define JSON_BROADCAST "broadcast"

struct SprocketMessage : public JsonStruct {
    String domain;
    String to;
    String from;
    String payload;
    int broadcast;
    String topic;

    // TODO do we even need that?
    enum SprocketMessageType { NONE, SYSTEM, APP, OTA } type;
    // ------------------------------------------------------------------------------------------
    int verifyJsonObject(JsonObject& json){
        return json.success()
            //&& json.containsKey(JSON_DOMAIN)
            //&& json.containsKey(JSON_TO)
            //&& json.containsKey(JSON_FROM)
            //&& json.containsKey(JSON_TYPE)
            && json.containsKey(JSON_TOPIC)
            && json.containsKey(JSON_PAYLOAD);
    };
    void mapJsonObject(JsonObject& root){
        root[JSON_DOMAIN] = domain;
        root[JSON_TO] = to;
        root[JSON_FROM] = from;
        root[JSON_PAYLOAD] = payload;
        root[JSON_TOPIC] = topic;
        root[JSON_TYPE] = type;
        root[JSON_BROADCAST] = broadcast;
    }
    // Map a json object to this struct.
    void fromJsonObject(JsonObject& json){
        if(!verifyJsonObject(json)){
            Serial.println("ERROR: cannot parse SprocketMessage JSON object");
            valid = 0;
            return;
        }
        domain = getAttrFromJson(json, JSON_DOMAIN);
        to = getAttrFromJson(json, JSON_TO);
        from = getAttrFromJson(json, JSON_FROM);
        payload = getAttrFromJson(json, JSON_PAYLOAD);
        topic = getAttrFromJson(json, JSON_TOPIC);
        broadcast = getIntAttrFromJson(json, JSON_BROADCAST);
        type = (SprocketMessageType) getIntAttrFromJson(json, JSON_TYPE);
        valid = 1;
    };
};

#endif