#ifndef __NETWORK_PLUGIN__
#define __NETWORK_PLUGIN__

#include "TaskSchedulerDeclarations.h"
#include "Plugin.h"
#include "Network.h"

class NetworkPlugin : public Plugin
{
  protected:
    Network *network;

  public:
    NetworkPlugin() {}
    NetworkPlugin(Network *net)
    {
        network = net;
    }

    void activate(Scheduler *userScheduler)
    {
        Serial.println("join network");
        network->init(userScheduler);
        network->connect();
    }
};

#endif