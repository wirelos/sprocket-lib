#ifndef __SPROCKET_H__
#define __SPROCKET_H__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION 
#define _TASK_PRIORITY

#include <Arduino.h>
#include <TaskSchedulerDeclarations.h>
#include <vector>
#include "FS.h"
#ifdef ESP32
#include "SPIFFS.h"
#endif
#include "Network.h"
#include "SprocketConfig.h"
#include "Plugin.h"

using namespace std;
using namespace std::placeholders;

class Sprocket : public EventChannel {
    protected:
        // TODO move scheduler out of Sprocket
        // => see difference between standalone and mesh sprochet usage of scheduler
        Scheduler* scheduler;
        Network network;
    private:
        SprocketMessage currentMessage;
    public:
        SprocketConfig config;
        std::vector<Plugin*> plugins;
        Sprocket();
        Sprocket(SprocketConfig);
        Sprocket* init(SprocketConfig);
        Sprocket* addTask(Task&);
        virtual void loop();
        virtual Sprocket* activate();
        virtual Sprocket* activate(Scheduler*) { return this; };
        virtual void dispatch( uint32_t from, String &msg );

        void addPlugin(Plugin* p);
        void activatePlugins(Scheduler* scheduler);
};

#endif