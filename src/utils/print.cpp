#include "utils/print.h"

int FORMAT_BUFFER_SIZE(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    int result = vsnprintf(NULL, 0, format, args);
    va_end(args);
    return result + 1; // safe byte for \0
}
void PRINT_MSG(Print &out, const char *prefix, const char *format, ...)
{
    if (SPROCKET_PRINT)
    {
        out.print(String(prefix) + String("\t| "));
        char formatString[128], *ptr;
        strncpy_P(formatString, format, sizeof(formatString)); // copy in from program mem
        // null terminate - leave last char since we might need it in worst case for result's \0
        formatString[sizeof(formatString) - 2] = '\0';
        ptr = &formatString[strlen(formatString) + 1]; // our result buffer...
        va_list args;
        va_start(args, format);
        vsnprintf(ptr, sizeof(formatString) - 1 - strlen(formatString), formatString, args);
        va_end(args);
        formatString[sizeof(formatString) - 1] = '\0';
        out.println(ptr);
    }
}