#ifndef __SPROCKET_UTILS__
#define __SPROCKET_UTILS__

#include <Arduino.h>

#ifndef SPROCKET_PRINT
#define SPROCKET_PRINT 1
#endif

// TODO move to sprocket

int FORMAT_BUFFER_SIZE(const char *format, ...);
void PRINT_MSG(Print &out, const char *prefix, const char *format, ...);

#endif