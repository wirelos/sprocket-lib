#ifndef __MISC_UTILS__
#define __MISC_UTILS__

#define ARRAY_LENGTH(array) sizeof(array)/sizeof(array[0])

static long mapValueToRange(long x, long in_min, long in_max, long out_min, long out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

#endif