#ifndef __SPROCKET_CONFIG__
#define __SPROCKET_CONFIG__

struct SprocketConfig {
    int startupDelay;
    int serialBaudRate;
};

#endif