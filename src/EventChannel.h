#ifndef __SPROCKET_EVENT_CHANNEL__
#define __SPROCKET_EVENT_CHANNEL__

#include <Arduino.h>
#include <ArduinoJson.h>
#include <list>
#include <functional>
#include <map>
#include <vector>

using namespace std;

typedef std::function<void(String msg)> subscriptionHandler_t;

class EventChannel {
    public:
        std::map<std::string, vector<subscriptionHandler_t>> subscriptions;
        void subscribe(String topic, subscriptionHandler_t handler) {
            subscriptions[topic.c_str()].reserve(1);
            subscriptions[topic.c_str()].push_back(handler);
        }
        void publish(String topic, String msg) {
            if (subscriptions.find(topic.c_str()) != subscriptions.end()){
                for(subscriptionHandler_t h : subscriptions[topic.c_str()]){
                    h(msg);
                }
            }
        }
};

#endif